## Sandbox for Instruction-Editing UI

To run locally:

- run `yarn start` from the project directory
- Open [http://localhost:3000](http://localhost:3000) to view app in the browser
