import React, { Component } from 'react';
import BodyField from './BodyField'

class StepFormContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.id,
      stepBody: '',
      errors: {}
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.bodyChangeHandler = this.bodyChangeHandler.bind(this)
    this.validateSubmission = this.validateSubmission.bind(this)
  }

  bodyChangeHandler(event) {
    this.setState( { stepBody: event.target.value } )
  }

  handleSubmit(event) {
    event.preventDefault()
    let payload = {
      body: this.state.stepBody,
      index: this.state.id
    }
    if (this.validateSubmission(payload.body)) {
      this.props.addNewStep(payload);
      this.setState( { stepBody: "" } )
    }
  }

  validateSubmission(submission) {
    let bodyPresent;
    if(submission.trim() === ""){
      let newError = {emptyBody: "You haven't typed anything!"};
      this.setState({errors: newError});
      bodyPresent = false;
    } else {
      let errorState = this.state.errors;
      delete errorState.emptyBody;
      this.setState({errors: errorState});
      bodyPresent = true;
    }
    return bodyPresent
  }

  render() {
    let errorItems;
    let errorDiv;
    if(Object.keys(this.state.errors).length > 0){
      errorItems = Object.values(this.state.errors).map(error =>{
        return(
          <li key={error}>{error}</li>
        );
      })
      errorDiv = <div>{errorItems}</div>;
    }
    return(
      <form onSubmit={this.handleSubmit} className="step-form callout">
        <BodyField
          content={this.state.stepBody}
          label=""
          name="step-body"
          handleChange={this.bodyChangeHandler}
        />
        <input className="button" type="submit" value="Submit" />
        {errorDiv}
      </form>
    )
  }
}

export default StepFormContainer
