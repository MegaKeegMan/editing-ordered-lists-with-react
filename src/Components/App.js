import React from 'react';
import '../App.css';
import StepsContainer from './StepsContainer.js';

const INSTRUCTIONS_LIST = ["Gather two pieces of bread, peanutbutter, jelly, and a butter knife.", "Spread a dollop of peanutbutter on one piece of bread.", "Spread a glob of jelly on the other piece of bread.", "Put the two pieces of bread together with the spreads facing each other.", "Enjoy your sandwich."];

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <StepsContainer
          steps={ INSTRUCTIONS_LIST }
        />
      </header>
    </div>
  );
}

export default App;
