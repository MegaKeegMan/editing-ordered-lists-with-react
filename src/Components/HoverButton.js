import React, { useState } from 'react'

const HoverButton = (props) => {
  const [button, showButton] = useState(false);
  return(
    <div>
      <span
        onMouseEnter={() => showButton(true)}
        onMouseLeave={() => showButton(false)}>
        --&nbsp;
        {button && (
          <button onClick={props.handleClick} tabIndex={props.id}>
          +
          </button>
        )}
        &nbsp;--
      </span>
    </div>
  )
}

export default HoverButton
