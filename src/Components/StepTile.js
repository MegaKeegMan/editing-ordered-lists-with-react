import React from 'react'

const StepTile = (props) => {
  return(
    <div>
      {props.orderNumber}) {props.body}
    </div>
  )
}

export default StepTile
