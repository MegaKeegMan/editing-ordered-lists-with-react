import React, { Component } from 'react'
import StepFormContainer from './StepFormContainer.js'
import HoverButton from './HoverButton.js'
import StepTile from './StepTile.js'

class StepsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      steps: this.props.steps,
      selectedId: null
    }
    this.addNewStep = this.addNewStep.bind(this)
    this.selectNewStepId = this.selectNewStepId.bind(this)
  }

  addNewStep(formPayload) {
        let currentSteps = this.state.steps
        let newStep = formPayload.body
        currentSteps.splice(formPayload.index, 0, newStep)
        let newList = currentSteps
        this.setState({
          steps: newList,
          selectedId: null
        })
  }

  selectNewStepId(newId) {
    this.setState({ selectedId: newId})
  }

  render() {
    let handleClick = (event) => {
      this.selectNewStepId(event.target.tabIndex)
    }
    let newItemDisplay = (index) => {
      if (this.state.selectedId === index) {
        return(
          <StepFormContainer
            addNewStep={this.addNewStep}
            id={index}
          />
        )
      } else {
        return(
          <HoverButton
            handleClick={handleClick}
            id={index}
          />
        )
      }
    }
    let steps = this.state.steps.map((step, index) => {
      return(
        <div key={index}>
          {newItemDisplay(index)}
          <StepTile
            body={step}
            orderNumber={index + 1}
          />
        </div>
      )
    })
    return(
      <div>
        <div>
          {steps}
        </div>
        {newItemDisplay(this.state.steps.length)}
      </div>
    )
  }
}

export default StepsContainer
